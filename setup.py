#!/usr/bin/env python

import sys

from setuptools import setup


PY3 = (sys.version_info[0] == 3)


setup(
    name="django-auth-test",
    version="0.0.1",
    description="Django minimal authentication backend test",
    long_description=open('README').read(),
    url="http://0.30000000000000004.com/",
    author="Nopo Nopi",
    author_email="nope@macc.che",
    license="BSD",
    packages=["django_auth_test"],
    classifiers=[
        "Framework :: Django",
    ],
    keywords=["django", "minimal", "authentication", "auth"],
    install_requires=[
        "django",
    ],
    setup_requires=[
        "setuptools >= 0.6c11",
    ]
)
