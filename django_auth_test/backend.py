from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User


class AuthBackend(object):
    """
    Authenticate against the settings AUTH_TEST['username'] and AUTH_TEST['password'].

    Use the login name and a hash of the password. For example:
    AUTH_TEST = {
        'username': 'admin',
        'password': 'pbkdf2_sha256$30000$Vo0VlMnkR4Bk$qEvtdyZRWTcOsCnI/oQ7fVOu1XAURIZYoOZ3iq8Dr4M='
    }
    """

    def authenticate(self, username=None, password=None):

        login_valid = (settings.AUTH_TEST.get('username') == username)
        pwd_valid = check_password(password, settings.AUTH_TEST.get('password'))
        if login_valid and pwd_valid:
            user, created = User.objects.get_or_create(username=username, is_staff=True, is_superuser=True)
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
